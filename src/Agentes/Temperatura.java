/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agentes;

import GUI.VentanaAgentesJF;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.awt.Color;


/**
 *
 * @author kfsarango1
 */
public class Temperatura extends Agent{

    
    
    @Override
    protected void setup() {
        addBehaviour( new TickerBehaviour(this, 3000) { //Ejecución cada cierto periodo
            @Override
            //Establece la acción de  comportamiento del agente
            protected void onTick() {
                
                //Modificando componentes de la interfaz VentanaAgentes, estado incial de la interfaz
                VentanaAgentesJF.air_conditionaterLBL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/minisplit_bn.png")));
                VentanaAgentesJF.tit_aireLBL.setForeground(Color.GRAY);
                VentanaAgentesJF.heatingLBL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/heating_bn.png")));
                VentanaAgentesJF.tit_calefaccionLBL.setForeground(Color.GRAY);
                VentanaAgentesJF.estadoTXF.setFont(new java.awt.Font("Ubuntu Mono", 0, 15));
                VentanaAgentesJF.tempJTBL.setBackground(null);
                
                //Se genera valores aleatorios de temperatura
                double temp = (double) (Math.random() * 57) + 1;
                
                //Convieritiendo el valor de aleatorio de double a string
                String temperatura = String.valueOf(temp);
                
                //Tipo de mensaje que se va a enviar
                ACLMessage message = new ACLMessage(ACLMessage.INFORM);
                //El contenido del mensaje sera la temperatura
                message.setContent(temperatura);
                //Indicando el agente que recibirá el mensaje | AgAcondicionador 
                //  corresponde a la clase Artefacto
                message.addReceiver(new AID("AgAcondicionador", AID.ISLOCALNAME));
                //Enviando el mensaje
                send(message);
            }
        });
    }
    
    
}
